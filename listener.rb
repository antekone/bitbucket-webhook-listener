require 'webrick'
require 'json'
require 'net/http'

require_relative 'handler_factory.rb'
require_relative 'result.rb'
require_relative 'bitbucket.rb'

require_relative 'config.rb'

def load_config
  JSON.load(File.open("config.json"))
end

def project_by_name(config, name)
  config['projects'].each do |project|
    return project if project['name'] == name
  end
  return nil
end

def read_creds_for_machine(name)
  IO.readlines("#{ENV["HOME"]}/.netrc").each do |line|
    line.strip!
    if line.start_with? "machine #{name} "
      if line =~ /login (.*?) password (.*)/i
        return [$1.strip, $2.strip]
      end
    end
  end

  return nil
end

def request_jenkins(name, token, arg_name, git_hash)
  uri = URI("#{JENKINS_URL}/job/#{name}/buildWithParameters?token=#{token}&#{arg_name}=#{git_hash}")
  puts "Will post URI: #{uri}"

  post = Net::HTTP::Post.new(uri)
  user, pass = read_creds_for_machine(uri.hostname)
  return [false, "Missing auth info for #{uri.hostname}"] if user == nil or pass == nil

  post.basic_auth user, pass
  res = Net::HTTP.start(uri.hostname, uri.port) do |http|
    ret = http.request(post)
    if ret.code.to_i >= 200 and ret.code.to_i < 300
      return [true, "OK"]
    else
      return [false, "Jenkins error: #{ret}"]
    end
  end
end

class HandleRoot < WEBrick::HTTPServlet::AbstractServlet
  def trigger_jenkins repo_name, git_hash
    project = project_by_name(load_config, repo_name)
    if project == nil
      return [false, "Error: config file doesn't define such project: #{repo_name}"]
    end

    jenkins_project_name = project['jen-job-name']
    jenkins_project_token = project['jen-job-token']

    git_commit_arg_name = project['git-commit-arg'] || 'COMMIT_ID'

    puts "Requesting jenkins build... #{git_commit_arg_name}=#{git_hash}, jenkins-ci-name: #{jenkins_project_name}"
    return request_jenkins jenkins_project_name, jenkins_project_token, git_commit_arg_name, git_hash
  end

  def error_response(resp, err_num, err_msg)
    resp.status = err_num
    resp.body = { "error" => err_num, "message" => err_msg }.to_json.to_s + "\n"
  end

  def success_response(resp, err_msg)
    resp.body = { "success" => 1, "message" => err_msg }.to_json.to_s + "\n"
  end

  def send_async_request build_object
    puts "Async request: #{build_object}"
    succ, errmsg = trigger_jenkins build_object.repo_name, build_object.hash
    if not succ
      puts "Jenkins error: #{errmsg}"
    else
      puts "Jenkins request OK"
    end
  end

  def process_request(req, resp)
    json_text = req.body
    HandlerFactory.by_request(req, json_text).extract_build_requests(json_text).if_err { |errmsg|
      error_response resp, 500, "Error in processing request: #{errmsg}"
      return
    }.if_ok { |array|
      array.map { |bobj|
        Thread.new {
          send_async_request bobj
        }
      }
    }

    success_response resp, "OK"
  end

  def do_POST(req, resp)
    begin
      if req.body != nil
        process_request(req, resp)
      else
        resp.body = "Test OK"
        resp.status = 200
      end
    rescue => e
      text = e.to_s

      resp.body = { "error" => 500,
                    "message" => "Exception when processing request",
                    "exception" => text }.to_json.to_s

      puts "Exception! #{resp.body}"
      resp.status = 500
      puts e.backtrace
    end
  end
end

http = WEBrick::HTTPServer.new(:Port => 1180)
http.mount "/", HandleRoot

trap("INT") do
  http.shutdown
end

http.start
