require_relative 'github.rb'
require_relative 'bitbucket.rb'

class HandlerFactory
  def HandlerFactory.by_request req, json_text
    return GitHub.new if req.header.key? 'x-github-event'
    return Bitbucket.new
  end
end
