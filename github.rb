require 'json'
require_relative 'result.rb'
require_relative 'build_object.rb'

class GitHub
  def extract_build_requests json_text
    return Result.err 'Invalid argument' if json_text == nil
    return Result.err 'Empty JSON' if json_text.empty?

    begin
      json_obj = JSON.parse json_text
    rescue Exception => e
      return Result.err "Invalid JSON (syntax)"
    end
    return Result.err 'Invalid JSON' if json_obj == nil

    self.parse_push_request json_obj
  end

  def parse_push_request push_req_obj
    last_commit = push_req_obj['after'] || (
      return Result.err "Missing after attribute"
    )

    repository = push_req_obj['repository'] || (
      return Result.err "Missing repository attribute"
    )

    full_name = repository['full_name'] || (
      return Result.err "Missing full_name attribute in repository"
    )

    Result.ok [BuildObject.new(full_name, last_commit)]
  end
end
