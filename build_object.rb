class BuildObject
  attr_accessor :repo_name, :hash
  def initialize repo_name, hash
    @repo_name = repo_name
    @hash = hash
  end
  def == other; other.repo_name == @repo_name and other.hash == @hash end
  def to_s; "BuildObject[repo_name='#{@repo_name}', hash='#{hash}']" end
end
