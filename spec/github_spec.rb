require_relative '../github.rb'
require_relative '../build_object.rb'

def read_whole(fn)
  IO.readlines(fn).each.to_a.join("")
end

RSpec.describe GitHub, "#test" do
  it "should return an error result on nil input" do
    b = GitHub.new
    r = b.extract_build_requests nil

    expect(r).to_not   be_nil
    expect(r).to       be_a(Result)
    expect(r.is_ok).to be false
  end

  it "should return an OK result on valid input (github.json)" do
    b = GitHub.new
    r = b.extract_build_requests read_whole("spec/github.json")

    expect(r).to_not    be_nil
    expect(r).to        be_a(Result)
    expect(r.is_ok).to  be true

    r.if_ok { |value|
      expect(value).to  be_a(Array)
      expect(value).to  include(BuildObject.new("Codertocat/Hello-World", "0000000000000000000000000000000000000000"))
      expect(value.size).to be 1
    }
  end

  it "should return a valid result on valid input (github2.json)" do
    b = GitHub.new
    r = b.extract_build_requests read_whole("spec/github2.json")

    expect(r).to_not    be_nil
    expect(r).to        be_a(Result)
    expect(r.is_ok).to  be true

    r.if_ok { |value|
      expect(value).to  be_a(Array)
      expect(value).to  include(BuildObject.new("antekone/libarchive", "d7ea622f97e82956170d5be1a43ad6802b6fb2d9"))
      expect(value.size).to be 1
    }
  end
end
