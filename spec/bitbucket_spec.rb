require_relative '../bitbucket.rb'
require_relative '../build_object.rb'

def read_whole(fn)
  IO.readlines(fn).each.to_a.join("")
end

RSpec.describe Bitbucket, "#test" do
  it "should return an error result on nil input" do
    b = Bitbucket.new
    r = b.extract_build_requests nil

    expect(r).to_not   be_nil
    expect(r).to       be_a(Result)
    expect(r.is_ok).to be false
  end

  it "should return an error result on empty input" do
    b = Bitbucket.new
    r = b.extract_build_requests ""

    expect(r).to_not    be_nil
    expect(r).to        be_a(Result)
    expect(r.is_ok).to  be false
  end

  it "should return an OK result on valid input (example)" do
    b = Bitbucket.new
    r = b.extract_build_requests read_whole("spec/example.json")

    expect(r).to_not    be_nil
    expect(r).to        be_a(Result)
    expect(r.is_ok).to  be true
  end

  it "should return an OK result on valid input (example2)" do
    b = Bitbucket.new
    r = b.extract_build_requests read_whole("spec/example2.json")

    expect(r).to_not    be_nil
    expect(r).to        be_a(Result)
    expect(r.is_ok).to  be true
  end

  it "should return a valid result on valid input (example2)" do
    b = Bitbucket.new
    r = b.extract_build_requests read_whole("spec/example2.json")

    expect(r).to_not    be_nil
    expect(r).to        be_a(Result)
    expect(r.is_ok).to  be true

    r.if_ok { |value|
      expect(value).to  be_a(Array)
      expect(value).to  include(BuildObject.new("full name", "1 one"))
      expect(value.size).to be 1
    }
  end
end
