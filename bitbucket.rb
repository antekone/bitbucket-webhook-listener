require 'json'
require_relative 'result.rb'
require_relative 'build_object.rb'

class Bitbucket
  # Returns Result[Array[BuildObject], String]
  def extract_build_requests json_text
    return Result.err 'Invalid argument' if json_text == nil
    return Result.err 'Empty JSON' if json_text.empty?

    begin
      json_obj = JSON.parse json_text
    rescue Exception => e
      return Result.err "Invalid JSON (syntax)"
    end
    return Result.err 'Invalid JSON' if json_obj == nil

    self.parse_notification json_obj
  end

  def parse_push_request push_req_obj
    repository_obj = push_req_obj['repository'] || (
      return Result.err 'Missing repository key'
    )

    full_name = repository_obj['full_name'] || (
      return Result.err 'Missing full_name in repository key'
    )

    changes_arr = push_req_obj['push']['changes'] || (
      return Result.err 'Missing changes array in push request'
    )

    items = []
    changes_arr.first(1).each { |change_obj|
      commits_arr = change_obj['commits'] || (
        return Result.err 'Missing commit info in a change object'
      )

      commits_arr.first(1).each { |commit_obj|
        hash = commit_obj['hash'] || (
          return Result.err 'Missing hash in a commit object'
        )

        items << BuildObject.new(full_name, hash)
      }
    }

    if items.empty?
      Result.err 'No build objects in this JSON'
    else
      Result.ok items
    end
  end

  def parse_notification json_obj
    if json_obj.key? 'push'
      parse_push_request json_obj
    else
      Result.err "Invalid request type; only 'push' request can be handled in this server version"
    end
  end
end
