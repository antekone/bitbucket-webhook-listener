class Result
  def initialize succ, value
    @succ = succ
    @value = value
  end

  def is_ok; @succ end
  def is_err; !@succ end

  def Result.ok ok_value
    Result.new true, ok_value
  end

  def Result.err err_value
    Result.new false, err_value
  end

  def map &block
    yield @value if @succ
    self
  end

  def map_err &err_block
    yield @value if not @succ
    self
  end

  attr_reader :value
  alias_method :if_ok, :map
  alias_method :if_err, :map_err
end

